package com.app.mobileapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.app.mobileapplication.model.UserDetails;
import com.app.mobileapplication.repository.UserRegistrationRepository;

class UserRegistrationServiceTest {

    @Mock
    private UserRegistrationRepository userRegistrationRepository;

    @InjectMocks
    private UserRegistrationService userRegistrationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateAccount() {
        UserDetails userDetails = new UserDetails(1, "John", "Doe", "johndoe", "password", "johndoe@example.com", 1234567890);
        userRegistrationService.createAccount(userDetails);
        // verify that the repository's save method was called once
        Mockito.verify(userRegistrationRepository, Mockito.times(1)).save(userDetails);
    }

    @Test
    void testCreateAccountWithNullUserDetails() {
        // assert that an IllegalArgumentException is thrown when the user details are null
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationService.createAccount(null);
        });
        assertEquals("UserDetails cannot be null.", exception.getMessage());
    }
}
