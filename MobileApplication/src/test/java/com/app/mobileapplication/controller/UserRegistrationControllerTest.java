package com.app.mobileapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.app.mobileapplication.model.UserDetails;
import com.app.mobileapplication.service.UserRegistrationService;

public class UserRegistrationControllerTest {
  
    @Mock
    private UserRegistrationService userRegistrationService;

    @InjectMocks
    private UserRegistrationController userRegistrationController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test //(To check the user registration process works correctly when valid user details are provided.)
    public void testUserRegistrationWithValidInputs() {
    	  UserDetails userDetails = new UserDetails(1, "John", "Doe", "johndoe", "password", "johndoe@example.com", 1234567890);

        doNothing().when(userRegistrationService).createAccount(any(UserDetails.class));

        String response = userRegistrationController.userRegistration(userDetails);

        assertEquals("Your registration is completed.", response);
        verify(userRegistrationService).createAccount(userDetails);
    }

    @Test //(To check when user missed userId field)
    void testUserRegistrationWithInvalidUserIdInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(0);
        userDetails.setFirstName("surya");
        userDetails.setLastName("chandra");
        userDetails.setUserName("surya76");
        userDetails.setEmail("surya76@gmail.com");
        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(939328229);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("User ID cannot be blank.", ex.getMessage());
    }

    @Test
    public void testUserRegistrationWithInvalidFirstNameInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(1);
        //userDetails.setFirstName("");
        userDetails.setLastName("Doe");
        userDetails.setUserName("johndoe");
        userDetails.setEmail("johndoe@example.com");
        userDetails.setPassword("password");
        userDetails.setMobileNumber(1234567890);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException msg = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });
        

        // Assert that the exception message is correct
        assertEquals("First Name cannot be blank.", msg.getMessage());
    }

    @Test //(To check when user missed userId field)
    void testUserRegistrationWithInvalidLastNameInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(283);
        userDetails.setFirstName("surya");
//        userDetails.setLastName("chandra");
        userDetails.setUserName("surya76");
        userDetails.setEmail("surya76@gmail.com");
        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(939328229);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("Last Name cannot be blank.", ex.getMessage());
    }
    
    @Test //(To check when user missed userId field)
    void testUserRegistrationWithInvalidUserNameInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(892);
        userDetails.setFirstName("surya");
        userDetails.setLastName("chandra");
//        userDetails.setUserName("surya76");
        userDetails.setEmail("surya76@gmail.com");
        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(939328229);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("User Name cannot be blank.", ex.getMessage());
    }
    
    @Test //(To check when user missed Email field)
    void testUserRegistrationWithInvalidEmailInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(373);
        userDetails.setFirstName("surya");
        userDetails.setLastName("chandra");
        userDetails.setUserName("surya76");
//        userDetails.setEmail("surya76@gmail.com");
        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(939328229);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("Email cannot be blank.", ex.getMessage());
    }
    
    @Test //(To check when user missed password field)
    void testUserRegistrationWithInvalidPasswordInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(283);
        userDetails.setFirstName("surya");
        userDetails.setLastName("chandra");
        userDetails.setUserName("surya76");
        userDetails.setEmail("surya76@gmail.com");
//        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(939328229);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("Password cannot be blank.", ex.getMessage());
    }

    @Test //(To check when user missed userId field)
    void testUserRegistrationWithInvalidMobileNumberInputs() {
        // Set up the invalid user details
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(631);
        userDetails.setFirstName("surya");
        userDetails.setLastName("chandra");
        userDetails.setUserName("surya76");
        userDetails.setEmail("surya76@gmail.com");
        userDetails.setPassword("sncjsn");
        userDetails.setMobileNumber(0);

        // Call the user registration controller method and expect an IllegalArgumentException to be thrown
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
            userRegistrationController.userRegistration(userDetails);
        });

        // Assert that the exception message is correct
        assertEquals("Mobile Number cannot be blank.", ex.getMessage());
    }


}
