package com.app.mobileapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.mobileapplication.model.UserDetails;
import com.app.mobileapplication.service.UserRegistrationService;

@RestController
@RequestMapping("/mobile")
public class UserRegistrationController {
	
	@Autowired
	private UserRegistrationService userRegistrationService;
	
	@PostMapping("/userregistration")
	public String userRegistration(@RequestBody UserDetails userDetails) {
	    validateRegistrationInputs(userDetails);
	    userRegistrationService.createAccount(userDetails);
	    return "Your registration is completed.";
	}

	private void validateRegistrationInputs(UserDetails userDetails) {
		validateInput("User ID", String.valueOf(userDetails.getUserId()));
	    validateInput("First Name", userDetails.getFirstName());
	    validateInput("Last Name", userDetails.getLastName());
	    validateInput("User Name", userDetails.getUserName());
	    validateInput("Email", userDetails.getEmail());
	    validateInput("Password", userDetails.getPassword());
	    validateInput("Mobile Number", String.valueOf(userDetails.getMobileNumber()));
	}

	private void validateInput(String fieldName, String fieldValue) {
	    if (fieldValue == null || fieldValue.equals("0")) {
	        throw new IllegalArgumentException(fieldName + " cannot be blank.");
	    }
	}
	
	
}
