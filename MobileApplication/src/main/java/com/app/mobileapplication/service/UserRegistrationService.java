package com.app.mobileapplication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.mobileapplication.model.UserDetails;
import com.app.mobileapplication.repository.UserRegistrationRepository;

@Service
public class UserRegistrationService {
	
	@Autowired
	private UserRegistrationRepository userRegistrationRepository;
	
	
	public void createAccount(UserDetails userDetails) {
	    if (userDetails == null) {
	        throw new IllegalArgumentException("UserDetails cannot be null.");
	    }
	    userRegistrationRepository.save(userDetails);
	}

	
}
