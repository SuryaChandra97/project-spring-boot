package com.app.mobileapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.mobileapplication.model.UserDetails;

@Repository
public interface UserRegistrationRepository extends JpaRepository<UserDetails, Integer>{

}
